# frozen_string_literal: true

RSpec.describe ExchangeIt::User do
  let(:user) { |ex| described_class.new(ex.metadata[:name] || 'Jhon', 'Smith') }
  let(:user_no_name) { described_class.new(nil, 'Smith') }
  let(:user_no_surname) { described_class.new('Jhon', nil) }

  it 'returns name' do # example, specify
    expect(user.name).to eq('Jhon')
    expect(user.name).to be_an_instance_of(String)
  end

  it 'returns surname' do
    expect(user.surname).to eq('Smith')
  end

  it 'returns name as a string', name: nil do
    expect(user.name).to be_an_instance_of(String)
  end

  it 'returns surname as a string' do
    expect(user_no_surname.name).to be_an_instance_of(String)
  end

  specify '#account' do
    expect(user.account).to be_an_instance_of(ExchangeIt::Account)
  end

  it 'has zero balance by default' do
    expect(user.balance).to eq(0)
  end
end
