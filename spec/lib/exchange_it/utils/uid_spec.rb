# frozen_string_literal: true

RSpec.describe ExchangeIt::Utils::Uid do
  # include described_class так лучше не делать
  let(:dummy) { Class.new { include ExchangeIt::Utils::Uid }.new }

  describe '#hash' do
    it 'returns nil when no args given' do
      expect(dummy.hash).to be_nil
    end

    it 'returns string when at least 1 arg was given' do
      hash = dummy.hash('str', 'str2')
      expect(dummy.hash('str', 'str2')).to be_an_instance_of(String)
      expect(dummy.hash('str', 'str2')).to eq(hash)
    end
  end
end
