### Использование тегов
запустить все тесты с тегом fast
```
rspec --tag fast
``` 

запустить все тесты без тега fast
```
rspec --tag "~fast"
```

Варианты записи тега
```
Первый вариант
it 'has zero balance', fast: true do
  expect(john.balance).to eq(0)
end

Второй варинат
it 'has zero balance', :fast do
  expect(john.balance).to eq(0)
end
```

### Запустить все тесты, где в описании есть name
```
rspec -e name 
```

### Использование метаданных в тесте
```
let(:user) { |ex| described_class.new(ex.metadata[:name] || 'Jhon', 'Smith') }


  it 'returns name as a string', name: nil do
    expect(user.name).to be_an_instance_of(String)
  end
```
### Получить метаданные в it
```
  it 'returns name as a string', name: nil do |ex|
    puts ex.metadata.inspect
    expect(user.name).to be_an_instance_of(String)
  end
```

### Запустить тесты, которые не прошли при прошлом запуске
```
rspec . --only-failures
```
предварительно нужно настроить конфиграцию rspec
```
RSpec.configure do |config|
  config.example_status_persistence_file_path = 'spec/specs.txt'
end
```

 ### Найти 3 самых медленных теста
 ```
 rspec . --profile 3
 ```

### Использование fit (focus: true) 
`fit - focus(short cut) - более важный тест(fspecify
fdescribe
fcontext)`
```
rspec --tag focus
```

```
fit 'returns name as a string' do
  expect(user.name).to be_an_instance_of(String)
end

fcontext 'returns name as a string' do
  expect(user.name).to be_an_instance_of(String)
end

fspecify 'returns name as a string' do
  expect(user.name).to be_an_instance_of(String)
end
```

### doubles, mocks, stubs, spies
Stub - возвращает заранее подготовленный результат

Mock - ожидает вызов метода, если вызова не происходит тест не пройдет

Null object - отвечает на любой вызов, всегда возвращает себя 
```
convert_stub = double.as_null_object
```

Spy - заглушка, может записывать сообщения, которые получает

привязать проверку к модулю, если в модуле нет метода, тест не пройдет
```
convert_stub = instance_double('ExchangeIt::Api::Converter', convert: 100 )
```

`allow` разрешает вызывать на объекте определенный метод

проверить что метод вызывается 

```
expect(convert_stub).to receive(:convert).with(sum: 80)
```
```
specify '#convert' do
    convert_stub = double.as_null_object
    стаб отвечает на метод и возвращает 100
    convert_stub = double('ExchangeIt::Api::Converter', convert: 100 )
    
    стап принимает аргумент sum: 80 и возвращает 100
    convert_stub = double('ExchangeIt::Api::Converter')
    
    allow(convert_stub).to receive(:convert).with(sum: 80).and_return(100)
    
    expect(convert_stub.convert(sum: 80)).to eq(100)

    метод конверт вызывался один раз с аргументом sum: 80
    expect(convert_stub).to have_received(:convert).with(sum: 80).once
  end
```